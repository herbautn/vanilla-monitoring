base:
  '*':
    - docker
    - iperf
    - iperf.client
    - monitoring.tickstack
  'h1':
    - iperf
    - iperf.server
