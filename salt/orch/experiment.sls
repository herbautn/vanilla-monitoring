install_docker:
  salt.state:
    - tgt: "*"
    - sls:
      - docker
      - dockerimg.unsecure_registry

pull_docker_images:
  salt.state:
    - tgt: 'h0'
    - sls:
      - dockerimg.load_registry

disseminate_docker_images:
  salt.state:
    - tgt: '*'
    - sls:
      - dockerimg.pull_local_registry

launch_tickstack:
  salt.state:
    - tgt: "h0"
    - sls:
      - monitoring.tickstack
      - monitoring.telegraf
      - monitoring.nftables


launch_iperf_server:
  salt.state:
    - tgt: "h1"
    - sls:
      - iperf
      - iperf.server

launch_iperf_client:
  salt.state:
    - tgt: "h[2-9]|[0-9]{2}"  #h2 to h99
    - tgt_type: pcre
    - sls: 
      - iperf
      - iperf.client


sync_modules:
  salt.function:
    - name: saltutil.sync_all
    - tgt: "*"

refresh_mine:
  salt.function:
    - name: mine.update
    - tgt: '*'

nftables:
  salt.state:
    - tgt: "*"
    - sls:
      - monitoring.nftables
