include:
  - iperf 


nherbaut/netcont-cli-running:
  docker_container.running:
    - command: bash -c "while true; do iperf -c {{salt["mine.get"]("h1","datapath_ip")["h1"][0]}} -p 8888; sleep 5; done; "
    - image: nherbaut/netcont
    - name: cli
    - requires: 
      - docker_image: nherbaut/netcont
