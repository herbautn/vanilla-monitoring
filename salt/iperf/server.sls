include:
  - iperf

nherbaut/netcont-srv-running:
  docker_container.running:
    - command: bash -c "iperf -s -p 8888"
    - image: nherbaut/netcont
    - name: srv
    - publish:
      - 0.0.0.0:8888:8888
    - requires: 
      - docker_image: nherbaut/netcont
