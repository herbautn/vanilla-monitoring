echo "delete previous data"
docker rm -f influxdb flowmatrix chronograf
echo "stop collecting data"
salt "*" cmd.run "service telegraf stop"
salt "*" mine.update
echo "deploy new nftables rules"
salt "*" state.apply 
