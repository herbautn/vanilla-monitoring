PRIVATE_KEY=$(cat settings.yaml |sed -n  "s/^ssh_key_file_private: \(.*\)/\1/p")
PUBLIC_KEY=$(cat settings.yaml |sed -n  "s/^ssh_key_file_public: \(.*\)/\1/p")


G5K_PRIVATE_KEY=$(cat settings.yaml |sed -n  "s/^g5k_ssh_key_file_private: \(.*\)/\1/p")
G5K_PUBLIC_KEY=$(cat settings.yaml |sed -n  "s/^g5k_ssh_key_file_public: \(.*\)/\1/p")

rm -f ./ssh_key_file_public ./ssh_key_file_private ./g5k_ssh_key_file_private ./g5k_ssh_key_file_public
cp $PUBLIC_KEY ./ssh_key_file_public
cp $PRIVATE_KEY ./ssh_key_file_private
cp $G5K_PRIVATE_KEY ./g5k_ssh_key_file_private
cp $G5K_PUBLIC_KEY ./g5k_ssh_key_file_public


docker build --no-cache   --build-arg USER=`sed -n  "s/^login: \(.*\)$/\1/p" ./settings.yaml` -t mcc .
