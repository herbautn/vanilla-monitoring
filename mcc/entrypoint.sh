#!/bin/bash


SITE=$(cat settings.yaml |sed -n  "s/default_site: \(.*\)/\1/p")
NODE_COUNT=$(cat settings.yaml |sed -n  "s/node_count: \(.*\)/\1/p")
WALL_TIME=$(cat settings.yaml |sed -n  "s/wall_time: \(.*\)/\1/p")

if [ -z "$JOBID" ]; then
  echo "creating job"
  JOBID=$(mcc job add $SITE $NODE_COUNT $WALL_TIME now)
fi
mkdir -p /root/cleanup
echo mcc job del $JOBID --site=$SITE >> /root/cleanup/cleanup.sh
echo "waiting for job $JOBID"
mcc job wait $JOBID
echo " done"
if [ -z "$DEPID" ]; then
echo "creating dep"
DEPID=$(mcc dep add $JOBID)
fi
echo "waiting for dep $DEPID"
mcc dep wait $DEPID
echo " done"
sleep 3
echo "installing salt on job $JOBID"
mcc job install $JOBID salt
mcc alias list $JOBID > /root/hosts
echo "source /root/hosts to get easy access"
exec mcc wait kill $JOBID
