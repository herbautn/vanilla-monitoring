

if [ $# -eq 4 ]; then

echo "container image name : " $1
echo "container image site : " $2
echo "container image count: " $3
echo "container image duration: " $4


./build1.sh
docker tag mcc:latest mcc-$1:latest
docker rmi -f mcc:latest

else

echo "usage: build.sh tgt_image_name site_name host_count duration_count"

fi
