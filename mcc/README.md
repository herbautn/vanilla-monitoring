# Goal

* provider a all-in-one mcc + salt + flowmatrix monitoring deployment for test purposes
* support only grid5000 for the moment

# Cinematics

* create a settings.yaml file from the provided template
* replace with your grid5000 login, password, crypto materials
* change the default file (where you want to allocate your nodes), walltime and node_count
* build the docker image with the provided build.sh

```
$> ./build.sj
```

* run the docker image

```bash
$> docker run --name nancy mcc
```

* if something goes wrong, you can take your deployement where you left it by profiding the JOBID and the DEPID env variables

```bash
$> docker run --name nancy -e JOBID=1321324 -e DEPID=dd4088ef-b313-434d-aefc-71642e3bf3ab run 
```

* attach to your container for easy login into your machines

```bash
$> docker exec  -ti nancy bash
%> . hosts
%> ssh0
+><F5>
```

* don't forget to delete your job when you're done

```
$> docker exec -e JOBID=1321324 nancy  bash -c "mcc job del $JOBID"
```



