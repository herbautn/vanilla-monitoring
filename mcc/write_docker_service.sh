#!/usr/bin/env bash
export USER=$1

echo "version: '3'"
echo "services:"

while read LINE; do
  IFS=\;  read NAME DEFAULT_SITE NODE_COUNT WALL_TIME <<< $LINE
  echo "  mcc-$NAME:"
  #echo "    image: mcc:$NAME"
  echo "    build: "
  echo "      context: ."

  echo "      args:"
  echo "        DEFAULT_SITE: $DEFAULT_SITE"
  echo "        NODE_COUNT: $NODE_COUNT"
  echo "        WALL_TIME: $WALL_TIME"
  echo "        USER: $USER"
done <recipe.txt
