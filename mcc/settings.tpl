login: <<grid5000 username>>
pwd: <<grid5000 password>>
api-backend: https://api.grid5000.fr/
ssh_key_file_public: << public rsa key used to connect to grid5000 access node>>
ssh_key_file_private: << private rsa key used to connect to grid500 access node>>
g5k_ssh_key_file_private: << private rsa key used to a grid5000 host >>
g5k_ssh_key_file_public: << private rsa key used to a grid5000 host >>
mailto: nicolas.herbaut@gmail.com
grid5k_ProxyCommand_domain_alias: g5k
environment: debian9-x64-base
default_site: nancy
default_queue: default
salt_host_control_iface: eth0
salt_host_data_iface: eth0
salt_minion_template: ./minion.tpl
salt_master_template: ./master.tpl
salt_states_repo_url: https://gricad-gitlab.univ-grenoble-alpes.fr/herbautn/vanilla-monitoring.git
salt_states_repo_branch: master
salt_state_dest_folder: /srv
salt_master_precommands:
  - apt-get update
  - apt-get install git python-pip --yes
  - pip install docker netifaces
salt_master_postcommands:
  - salt-run state.orchestrate orch.experiment
salt_minion_precommands:
  - apt-get update
  - apt-get install git python-pip --yes
  - pip install docker netifaces
salt_minion_postcommands: []

node_count: 3
wall_time: until 22h00
